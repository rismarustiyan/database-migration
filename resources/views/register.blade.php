<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>latihan Pekan 1.1 Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/register" method="POST">
        @csrf
        <label>First Name:</label><br><br>
            <input type="text" name="first_name"><br><br>
        <label>Last Name:</label><br><br>
            <input type="text" name="last_name"><br><br>
                <label>Gender:</label><br><br>
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label><br><br>
        <label>Nationality:</label><br><br>
            <select name=”nationality”>
                <option value=”indonesian”>Indonesian</option>
                <option value=”singaporean”>Singaporean</option>
                <option value=”Malaysian”>Malaysian</option>
                <option value=”Australian”>Australian</option>
            </select><br><br>
        <label>Language Spoken:</label><br>
            <input type="checkbox" name="bahasa_indo" value="1">Bahasa Indonesian<br>
            <input type="checkbox" name="english" value="2">English<br>
            <input type="checkbox" name="other" value="3">Other<br><br>
        <label>Bio:</label><br><br>
            <textarea name="bio" cols="35" rows="10"></textarea><br>
            <button type="submit">Sign Up</button>

    </form>
</body>
</html>